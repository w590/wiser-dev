# BooksList

*BooksList* - приложения для ведения списка книг. С ним вы сможете спланировать своё чтение и отслеживать ваш прогресс.

## Структура

### wiser-server

*wiser-server* - api с которым должны взаимодействовать клиенты. [Ссылка](https://gitlab.com/w590/wiser-server).

### wiser-client

*wiser-client* - браузерный клиент. [Ссылка](https://gitlab.com/w590/wiser-client).

### wiser-dev

*wiser-dev* - вариант развёрстки приложения в среде разработки.

### Другое

В проекте используется **traefik** в качестве обратного прокси-сервера и **postgresql** в качестве СУБД.

## Установка

```
git clone https://gitlab.com/w590/wiser-dev.git
git clone https://gitlab.com/w590/wiser-server.git
git clone https://gitlab.com/w590/wiser-client.git
cd wiser-dev
docker-compose up -d
```

Клиент находится на *http://localhost:80*, а api на *http://localhost:5000*.

## Авторы

1. Антонов Михаил. 26 группа.
